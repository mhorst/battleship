﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip
{
    class Ship
    {
        public int length = 0;
        public int[] start = {0,0};
        public int[] end = { 0, 0 };
        public bool[] strike = new bool[99];
         public bool[][] coordinates= new bool[99][];

        public Ship(int startx, int starty, int endx, int endy) {
            this.start[0] = startx;
            this.start[1] = starty;

            this.end[0] = endx;
            this.end[1] = endy;
            int tmp = (startx - endx) ^ 2 + (starty - endy) ^ 2;
            this.length = (int)Math.Sqrt(tmp);
            this.setCoordinates();
            for (int i = 0; i < this.length; i++)
            {
                this.strike[i] = false;
            }
        }

        public bool fire(int x, int y)
        {
            return this.isInShip(x,y);
        }

       
        public bool setCoordinates()
        {
           
            
            int m = 0;
            if (this.end[1] == this.start[1] || this.end[0] == this.start[0])
            {
                m = 0;
            }
            else
            {
                m = (this.end[1] - this.start[1]) / (this.end[0] - this.start[0]);
            }
            int n = this.start[1]-m*this.start[0];
           int tmpX;
			int tmpY;
			int tmpYEnd;
			int tmpXEnd;
			if(this.start[0]>this.end[0]) {
				tmpX = this.end[0];
				tmpXEnd = this.start[0];
			}
			else {
				tmpX = this.start[0];
                tmpXEnd = this.end[0];
			}
			if(this.start[1]>this.end[1]) {
				tmpY = this.end[1];
				tmpYEnd = this.start[1];
			}
			else {
				tmpY = this.start[1];
				tmpYEnd = this.end[1];
			}
            int posY;

            for (int i = 0; i < 99; i++)
            {
                this.coordinates[i] = new bool[99];
                for (int j = 0; j < 99; j++) {
                    posY = m * j + n;
                    if (posY == i && i <= tmpXEnd && i >= tmpX)
                        this.coordinates[i][j] = true;
                    else
                        this.coordinates[i][j] = false;
                }
            }

            return false;
        }

        public bool isInShip(int x, int y) {
            return this.coordinates[y][x];
        }

        
    }
}
