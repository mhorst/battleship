﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip
{
    class Control
    {
        public String content;
        public View view;
        public Ship[] ship;
        public Gamefield field;

        public Control() {
            this.view = new View();
            this.field = new Gamefield(10, 10);
        }

        public void mode(char input) {
            switch (input) { 
                case '1':
                    this.content = "Starte Spiel";
                    this.field.drawField();    
                    break;
                case 'a':
                    Console.WriteLine("\nY1 Position:");
                    ConsoleKeyInfo x1 = Console.ReadKey();
                    Console.WriteLine("\nX1 Position:");
                    ConsoleKeyInfo y1 = Console.ReadKey();
                    Console.WriteLine("\nY2 Position:");
                    ConsoleKeyInfo x2 = Console.ReadKey();
                    Console.WriteLine("\nX2 Position:");
                    ConsoleKeyInfo y2 = Console.ReadKey();
                    Ship tmpAdd = new Ship(Convert.ToInt32(x1.KeyChar) - 48, Convert.ToInt32(y1.KeyChar) - 48, Convert.ToInt32(x2.KeyChar) - 48, Convert.ToInt32(y2.KeyChar) - 48);
                    this.field.addShip(tmpAdd);
                    this.field.drawField();  
                    break;
                default:
                    break;
            }
            this.view.print(content);
            
        }
    }
}
