﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Control game = new Control();
            Console.WriteLine("Um Spiel zu starten Drücke eine Taste, um es zu beenden drücke die 0");
            ConsoleKeyInfo info = Console.ReadKey();
            while (info.KeyChar != '0')
            {
                info = Console.ReadKey();
                game.mode(info.KeyChar);
            }
        }
    }
}
