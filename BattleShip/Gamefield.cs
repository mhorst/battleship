﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip
{
    class Gamefield
    {
        public int x;
        public int y;
        public char[][] field = new char[999][];
        
        public Ship[] ship = new Ship[99];
        public int NumShips = 0;


        public Gamefield(int x, int y)
        {
            this.x = x;
            this.y = y;
            Console.WriteLine("Spielfeld erstellen...");
               
            for (int i = 0; i <= this.y; i++) {
                this.field[i] = new char[999];
                for (int j = 0; j <= this.x; j++) {
                    this.field[i][j] = 'X';
                }
            }
        }

        public void addShip(Ship ship) {
            this.ship[this.NumShips] = ship;
            this.NumShips++;
        }

        public void drawField() {
            bool firstrow = true;
            Console.WriteLine("\n_________________________________________\n");
            Console.Write("\n\t ");
            for (int i = -1; i <= this.y; i++) {
                for (int j = 0; j <= this.x; j++) {
                    if (firstrow == true)
                    {
                        Console.Write("x"+j+" ");
                    }
                    else {
                        if (this.isShip(j, i)) {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.Write(" # ");
                            Console.ResetColor();
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Blue;
                            Console.Write(" x ");
                            Console.ResetColor();
                        }   
                     }
                }
                firstrow = false;

                Console.Write("\n\ny" + (i+1) + "\t ");
            }
            Console.WriteLine("\n_________________________________________");
        }

        public bool isShip(int x, int y)
        {
            bool res = false;
            for (var i = 0; i < this.NumShips ;i++ )
            {
                res = this.ship[i].isInShip(x, y);
                if (res == true) {
                    return true;
                }
            }
            return res;
            
        }

    }
}
